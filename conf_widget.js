import CalendarHeatmapWidget from './path/to/CalendarHeatmapWidget';

const widgetsConfig = {
    'calendarHeatmap': {
        component: CalendarHeatmapWidget,
        title: 'Calendario de Calor',
        // Otros parámetros de configuración si es necesario
    }
};
