import dash
from dash import dcc, html
import plotly.express as px
import pandas as pd
import requests

# Fetch WFS data
wfs_url = 'http://geoserver.girsar/geoserver/MODIS_AQUA/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=MODIS_AQUA:MODIS_AQUA&outputFormat=application/json'
params = {
    'service': 'WFS',
    'version': '1.0.0',
    'request': 'GetFeature',
    'typeName': 'MODIS_AQUA:MODIS_AQUA',  # Reemplaza con el nombre de tu capa
    'outputFormat': 'application/json'
}
response = requests.get(wfs_url, params=params)
data = response.json()

# Process data
records = []
for feature in data['features']:
    props = feature['properties']
    records.append({
        'Fecha': props['Fecha'],
        'FP_POWER': props['FP_POWER']
    })

df = pd.DataFrame(records)

# Create the heatmap calendar figure
fig = px.density_heatmap(df, x='Fecha', y='FP_POWER', nbinsx=365, title='Calendario de Calor')

# Initialize the Dash app
app = dash.Dash(__name__)

app.layout = html.Div([
    html.H1("Gráfico de Calendario con Dash y Plotly"),
    dcc.Graph(figure=fig)
])

if __name__ == '__main__':
    app.run_server(debug=True)
